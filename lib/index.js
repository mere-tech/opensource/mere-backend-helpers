"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
var _exportNames = {
  empty: true
};
exports.empty = void 0;

var _components = require("./components");

Object.keys(_components).forEach(function (key) {
  if (key === "default" || key === "__esModule") return;
  if (Object.prototype.hasOwnProperty.call(_exportNames, key)) return;
  Object.defineProperty(exports, key, {
    enumerable: true,
    get: function get() {
      return _components[key];
    }
  });
});

var empty = function empty() {};

exports.empty = empty;