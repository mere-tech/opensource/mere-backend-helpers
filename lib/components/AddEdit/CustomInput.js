"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _react = _interopRequireDefault(require("react"));

var _FormControl = _interopRequireDefault(require("@material-ui/core/FormControl"));

var _InputLabel = _interopRequireDefault(require("@material-ui/core/InputLabel"));

var _FormHelperText = _interopRequireDefault(require("@material-ui/core/FormHelperText"));

var _Input = _interopRequireDefault(require("@material-ui/core/Input"));

/* eslint-disable react/prop-types */
// @material-ui/core components
function CustomInput(_ref) {
  var props = (0, _extends2["default"])({}, _ref);
  var formControlProps = props.formControlProps,
      labelText = props.labelText,
      id = props.id,
      labelProps = props.labelProps,
      inputProps = props.inputProps,
      helpText = props.helpText;
  return _react["default"].createElement(_FormControl["default"], formControlProps, labelText !== undefined ? _react["default"].createElement(_InputLabel["default"], (0, _extends2["default"])({
    htmlFor: id
  }, labelProps), labelText) : null, _react["default"].createElement(_Input["default"], (0, _extends2["default"])({
    id: id
  }, inputProps)), helpText !== undefined ? _react["default"].createElement(_FormHelperText["default"], {
    id: "".concat(id, "-text")
  }, helpText) : null);
}

var _default = CustomInput;
exports["default"] = _default;