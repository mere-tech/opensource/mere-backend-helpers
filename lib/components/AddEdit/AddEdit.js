"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _classCallCheck2 = _interopRequireDefault(require("@babel/runtime/helpers/classCallCheck"));

var _createClass2 = _interopRequireDefault(require("@babel/runtime/helpers/createClass"));

var _possibleConstructorReturn2 = _interopRequireDefault(require("@babel/runtime/helpers/possibleConstructorReturn"));

var _getPrototypeOf3 = _interopRequireDefault(require("@babel/runtime/helpers/getPrototypeOf"));

var _assertThisInitialized2 = _interopRequireDefault(require("@babel/runtime/helpers/assertThisInitialized"));

var _inherits2 = _interopRequireDefault(require("@babel/runtime/helpers/inherits"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var React = _interopRequireWildcard(require("react"));

var _Drawer = _interopRequireDefault(require("@material-ui/core/Drawer"));

var _Form = _interopRequireDefault(require("./Form"));

var _styles = require("@material-ui/core/styles");

/*eslint-disable */
var styles = {
  paperAnchorRight: {
    padding: 100,
    minWidth: 650,
    maxWidth: 1000
  }
};

var SimpleDialog =
/*#__PURE__*/
function (_React$Component) {
  (0, _inherits2["default"])(SimpleDialog, _React$Component);

  function SimpleDialog() {
    var _getPrototypeOf2;

    var _this;

    (0, _classCallCheck2["default"])(this, SimpleDialog);

    for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    _this = (0, _possibleConstructorReturn2["default"])(this, (_getPrototypeOf2 = (0, _getPrototypeOf3["default"])(SimpleDialog)).call.apply(_getPrototypeOf2, [this].concat(args)));
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "state", {
      showDialog: false
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "showDialogCallback", function () {
      _this.setState({
        showDialog: true
      });
    });
    (0, _defineProperty2["default"])((0, _assertThisInitialized2["default"])(_this), "handleClose", function () {
      var onClose = _this.props.onClose;

      if (onClose) {
        onClose();
      }

      _this.setState({
        showDialog: false
      });
    });
    return _this;
  }

  (0, _createClass2["default"])(SimpleDialog, [{
    key: "render",
    value: function render() {
      var _this$props = this.props,
          title = _this$props.title,
          editType = _this$props.editType,
          trigger = _this$props.trigger,
          header = _this$props.header;
      var showDialog = this.state.showDialog;
      return React.createElement("span", null, trigger(this.showDialogCallback), showDialog && React.createElement(_Drawer["default"], {
        classes: this.props.classes,
        anchor: "right",
        open: showDialog
      }, React.createElement("h4", null, title), header, React.createElement(_Form["default"], (0, _extends2["default"])({}, this.props, {
        showClose: true,
        handleClose: this.handleClose
      }))));
    }
  }]);
  return SimpleDialog;
}(React.Component);

var _default = (0, _styles.withStyles)(styles)(SimpleDialog);

exports["default"] = _default;