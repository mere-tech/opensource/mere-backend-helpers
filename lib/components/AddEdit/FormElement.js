"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var React = _interopRequireWildcard(require("react"));

var _reduxForm = require("redux-form");

var _FormControl = _interopRequireDefault(require("@material-ui/core/FormControl"));

var _FormControlLabel = _interopRequireDefault(require("@material-ui/core/FormControlLabel"));

var _InputLabel = _interopRequireDefault(require("@material-ui/core/InputLabel"));

var _Checkbox = _interopRequireDefault(require("@material-ui/core/Checkbox"));

var _Check = _interopRequireDefault(require("@material-ui/icons/Check"));

var _Select = _interopRequireDefault(require("@material-ui/core/Select"));

var _MenuItem = _interopRequireDefault(require("@material-ui/core/MenuItem"));

var _CustomInput = _interopRequireDefault(require("./CustomInput"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var filterChange = function filterChange(event) {
  return (// eslint-disable-next-line no-console
    console.log(event.target.value)
  );
};

var renderCheckbox = function renderCheckbox(_ref) {
  var input = _ref.input,
      placeholder = _ref.placeholder,
      classes = _ref.classes,
      defaultValue = _ref.defaultValue;
  return React.createElement("div", {
    className: classes.checkboxAndRadio
  }, React.createElement(_FormControlLabel["default"], {
    control: React.createElement(_Checkbox["default"], {
      inputProps: _objectSpread({}, input),
      checked: input.value !== '' ? input.value : defaultValue,
      value: input.value,
      tabIndex: -1,
      onClick: function onClick(param) {
        return input.onChange(param);
      },
      checkedIcon: React.createElement(_Check["default"], {
        className: classes.checkedIcon
      }),
      icon: React.createElement(_Check["default"], {
        className: classes.uncheckedIcon
      }),
      classes: {
        checked: classes.checked
      }
    }),
    label: placeholder
  }));
};

var renderSelectField = function renderSelectField(_ref2) {
  var input = _ref2.input,
      label = _ref2.label,
      _ref2$meta = _ref2.meta,
      touched = _ref2$meta.touched,
      error = _ref2$meta.error,
      children = _ref2.children,
      custom = (0, _objectWithoutProperties2["default"])(_ref2, ["input", "label", "meta", "children"]);
  return React.createElement(_FormControl["default"], {
    error: touched && error
  }, React.createElement(_InputLabel["default"], {
    htmlFor: "tag-type-native-simple"
  }, "Tag Type:"), React.createElement(_Select["default"], (0, _extends2["default"])({
    "native": true
  }, input, custom, {
    inputProps: {
      name: 'tag-type',
      id: 'tag-type-native-simple'
    }
  }), children));
}; // const renderSelect = ({ placeholder, input, defaultValue, value }: any): React.Node => (
//   <Select
//     labelId="demo-simple-select-label"
//     id="demo-simple-select"
//     value={defaultValue}
//     onChange={(param) => {
//       input.onChange(console.log(param));
//     }}
//     a={console.log(value)}
//     b={console.log(placeholder)}
//     c={console.log(input)}
//     d={console.log(defaultValue)}
//   >
//     <MenuItem value="General:">General</MenuItem>
//     <MenuItem value="Type:">Type</MenuItem>
//     <MenuItem value="Author:">Author</MenuItem>
//     <MenuItem value="Application:">Application</MenuItem>
//     <MenuItem value="Discipline:">Discipline</MenuItem>
//   </Select>
// );


var renderField = function renderField(_ref3) {
  var placeholder = _ref3.placeholder,
      input = _ref3.input,
      defaultValue = _ref3.defaultValue,
      value = _ref3.value;
  return React.createElement(_CustomInput["default"], {
    labelText: placeholder,
    inputProps: {
      value: value,
      onChange: function onChange(param) {
        return input.onChange(param);
      },
      defaultValue: defaultValue
    },
    formControlProps: {
      fullWidth: true
    }
  });
};

var renderIntegerField = function renderIntegerField(_ref4) {
  var placeholder = _ref4.placeholder,
      input = _ref4.input;
  return React.createElement(_CustomInput["default"], {
    labelText: placeholder,
    inputProps: {
      value: input.value,
      onChange: function onChange(param) {
        return input.onChange(param);
      }
    },
    formControlProps: {
      fullWidth: true
    }
  });
};

var RenderFormElement = function RenderFormElement(_ref5) {
  var name = _ref5.name,
      type = _ref5.type,
      valueKey = _ref5.valueKey,
      value = _ref5.value;

  switch (type) {
    case 'select':
      return React.createElement(_reduxForm.Field, {
        label: "Favorite Color",
        name: valueKey,
        type: "select",
        component: renderSelectField,
        defaultValue: value,
        placeholder: name
      }, React.createElement("option", {
        value: ""
      }, " "), React.createElement("option", {
        value: "Action:"
      }, "Action:"), React.createElement("option", {
        value: "Application:"
      }, "Application:"), React.createElement("option", {
        value: "Author:"
      }, "Author:"), React.createElement("option", {
        value: "Command:"
      }, "Command:"), React.createElement("option", {
        value: "Course:"
      }, "Course:"), React.createElement("option", {
        value: "General:"
      }, "General:"), React.createElement("option", {
        value: "Discipline:"
      }, "Discipline:"), React.createElement("option", {
        value: "Program:"
      }, "Program:"), React.createElement("option", {
        value: "Tool:"
      }, "Tool:"), React.createElement("option", {
        value: "Type:"
      }, "Type:"));

    case 'string':
      return React.createElement(_reduxForm.Field, {
        name: valueKey,
        type: "text",
        component: renderField,
        placeholder: name,
        defaultValue: value
      });

    case 'integer':
      return React.createElement(_reduxForm.Field, {
        name: valueKey,
        type: "number",
        format: function format(val) {
          return val ? val.toString() : '0';
        },
        parse: function parse(val) {
          return Number.isNaN(parseInt(val, 10)) ? null : parseInt(val, 10);
        },
        component: renderIntegerField,
        placeholder: name,
        defaultValue: value
      });

    case 'boolean':
      return React.createElement(_reduxForm.Field, {
        type: "text",
        name: valueKey,
        component: renderCheckbox,
        placeholder: name,
        defaultValue: value
      });
    // renderField();
    // return (
    //   <GridContainer>
    //     <ItemGrid xs={12} sm={12} md={3}>
    //       <FormLabel>
    //         {name}
    //       </FormLabel>
    //     </ItemGrid>
    //     <ItemGrid xs={12} sm={12} md={9}>
    //       <CustomInput
    //         id={id}
    //         value={defaultProp}
    //         formControlProps={{
    //           fullWidth: true,
    //         }}
    //       />
    //     </ItemGrid>
    //   </GridContainer>
    // );

    default:
      return null;
  }
};

var _default = RenderFormElement;
exports["default"] = _default;