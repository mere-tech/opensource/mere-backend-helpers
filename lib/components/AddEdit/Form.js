"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var React = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _reduxForm = require("redux-form");

var _FormElement = _interopRequireDefault(require("./FormElement"));

var mapValues = function mapValues(initialValues, field) {
  if (initialValues && initialValues[field.valueKey]) {
    return initialValues[field.valueKey];
  }

  return '';
};

var Form = function Form() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      inputs = _ref.inputs,
      mutation = _ref.mutation,
      translation = _ref.translation,
      translateFilter = _ref.translateFilter,
      editType = _ref.editType,
      handleClose = _ref.handleClose,
      buttons = _ref.buttons,
      showClose = _ref.showClose,
      refetchQueries = _ref.refetchQueries,
      initialValues = _ref.initialValues,
      onCompleted = _ref.onCompleted,
      client = _ref.client,
      success = _ref.success;

  var onSubmit = function onSubmit(x) {
    var toSub = x;

    if (initialValues) {
      toSub = Object.assign({}, initialValues, x);
    }

    var _toSub = toSub,
        __typename = _toSub.__typename,
        rest = (0, _objectWithoutProperties2["default"])(_toSub, ["__typename"]);

    if (rest.filter !== undefined) {
      rest.name = "".concat(rest.filter, " ").concat(rest.name);
    }

    var mutationParam = {
      mutation: mutation,
      variables: translation(rest)
    };
    var thisType = {
      editType: editType
    };

    if (thisType.editType === 'editTags') {
      client.mutate(mutationParam).then(function (response) {
        if (rest.filter !== undefined && rest.filter !== 'General:') {
          var newRest = translateFilter(rest);
          newRest.tagLinkInput.name = 'Filters';
          newRest.tagLinkInput.childTagId = response.data.addTagTo.tagId;
          client.mutate({
            mutation: mutation,
            variables: newRest
          }).then(handleClose());
        } else {
          handleClose();
        }
      });
    } else {
      client.mutate(mutationParam).then(onCompleted);
      handleClose();
    } // client.mutate(mutationParam).then((response) => {
    //   const thisType = { editType };
    //   if (thisType.editType === 'editTags') {
    //     if (rest.filter !== undefined && rest.filter !== 'General:') {
    //       const newRest = translateFilter(rest);
    //       newRest.tagLinkInput.name = 'Filters';
    //       newRest.tagLinkInput.childTagId = response.data.addTagTo.tagId;
    //       client.mutate({ mutation, variables: newRest }).then(handleClose());
    //     } else {
    //       handleClose();
    //     }
    //   } else {
    //     handleClose();
    //   }
    // });

  };

  var FormToRender = function FormToRender(_ref2) {
    var handleSubmit = _ref2.handleSubmit;
    return React.createElement("form", {
      onSubmit: handleSubmit(onSubmit)
    }, inputs.map(function (input) {
      return React.createElement(_FormElement["default"], (0, _extends2["default"])({
        key: input.id
      }, input, {
        value: mapValues(initialValues, input)
      }));
    }), buttons, !buttons && React.createElement("div", null, showClose && React.createElement("button", {
      type: "button",
      onClick: handleClose,
      color: "primary"
    }, "Cancel"), React.createElement("button", {
      type: "submit",
      color: "primary",
      autoFocus: true
    }, "Submit")));
  };

  var RedForm = (0, _reduxForm.reduxForm)({
    form: "add".concat(Date.now()),
    initialValues: initialValues,
    destroyOnUnmount: false,
    enableReinitialize: true
  })(FormToRender);
  return React.createElement(RedForm, null);
};

var _default = Form;
exports["default"] = _default;