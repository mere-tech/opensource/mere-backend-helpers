"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "AddEdit", {
  enumerable: true,
  get: function get() {
    return _AddEdit["default"];
  }
});
Object.defineProperty(exports, "Marker", {
  enumerable: true,
  get: function get() {
    return _Marker["default"];
  }
});
Object.defineProperty(exports, "MarkerAddToVideo", {
  enumerable: true,
  get: function get() {
    return _MarkerAddToVideo["default"];
  }
});
Object.defineProperty(exports, "MarkerRemove", {
  enumerable: true,
  get: function get() {
    return _MarkerRemove["default"];
  }
});
Object.defineProperty(exports, "Tag", {
  enumerable: true,
  get: function get() {
    return _Tag["default"];
  }
});
Object.defineProperty(exports, "TagAddToMarker", {
  enumerable: true,
  get: function get() {
    return _TagAddToMarker["default"];
  }
});
Object.defineProperty(exports, "TagRemove", {
  enumerable: true,
  get: function get() {
    return _TagRemove["default"];
  }
});
Object.defineProperty(exports, "TagUnlink", {
  enumerable: true,
  get: function get() {
    return _TagUnlink["default"];
  }
});
Object.defineProperty(exports, "TagUnlinkMarker", {
  enumerable: true,
  get: function get() {
    return _TagUnlinkMarker["default"];
  }
});
Object.defineProperty(exports, "File", {
  enumerable: true,
  get: function get() {
    return _File["default"];
  }
});
Object.defineProperty(exports, "FileRemove", {
  enumerable: true,
  get: function get() {
    return _FileRemove["default"];
  }
});
Object.defineProperty(exports, "FileAddToVideo", {
  enumerable: true,
  get: function get() {
    return _FileAddToVideo["default"];
  }
});
Object.defineProperty(exports, "FileAddToMarker", {
  enumerable: true,
  get: function get() {
    return _FileAddToMarker["default"];
  }
});
Object.defineProperty(exports, "Faq", {
  enumerable: true,
  get: function get() {
    return _Faq["default"];
  }
});
Object.defineProperty(exports, "FaqAddToVideo", {
  enumerable: true,
  get: function get() {
    return _FaqAddToVideo["default"];
  }
});
Object.defineProperty(exports, "FaqRemove", {
  enumerable: true,
  get: function get() {
    return _FaqRemove["default"];
  }
});
Object.defineProperty(exports, "Related", {
  enumerable: true,
  get: function get() {
    return _Related["default"];
  }
});
Object.defineProperty(exports, "RelatedRemove", {
  enumerable: true,
  get: function get() {
    return _RelatedRemove["default"];
  }
});
Object.defineProperty(exports, "RelatedAddToVideo", {
  enumerable: true,
  get: function get() {
    return _RelatedAddToVideo["default"];
  }
});

var _AddEdit = _interopRequireDefault(require("./AddEdit/AddEdit"));

var _Marker = _interopRequireDefault(require("./Marker/Marker"));

var _MarkerAddToVideo = _interopRequireDefault(require("./Marker/MarkerAddToVideo"));

var _MarkerRemove = _interopRequireDefault(require("./Marker/MarkerRemove"));

var _Tag = _interopRequireDefault(require("./Tag/Tag"));

var _TagAddToMarker = _interopRequireDefault(require("./Tag/TagAddToMarker"));

var _TagRemove = _interopRequireDefault(require("./Tag/TagRemove"));

var _TagUnlink = _interopRequireDefault(require("./Tag/TagUnlink"));

var _TagUnlinkMarker = _interopRequireDefault(require("./Tag/TagUnlinkMarker"));

var _File = _interopRequireDefault(require("./File/File"));

var _FileRemove = _interopRequireDefault(require("./File/FileRemove"));

var _FileAddToVideo = _interopRequireDefault(require("./File/FileAddToVideo"));

var _FileAddToMarker = _interopRequireDefault(require("./File/FileAddToMarker"));

var _Faq = _interopRequireDefault(require("./Faq/Faq"));

var _FaqAddToVideo = _interopRequireDefault(require("./Faq/FaqAddToVideo"));

var _FaqRemove = _interopRequireDefault(require("./Faq/FaqRemove"));

var _Related = _interopRequireDefault(require("./Related/Related"));

var _RelatedRemove = _interopRequireDefault(require("./Related/RelatedRemove"));

var _RelatedAddToVideo = _interopRequireDefault(require("./Related/RelatedAddToVideo"));