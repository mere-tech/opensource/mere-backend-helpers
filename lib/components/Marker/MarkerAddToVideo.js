"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var React = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _Marker = _interopRequireDefault(require("./Marker"));

var mutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "addMediaAsset" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "input" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "MediaAssetInput" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "addMediaAsset" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "mediaAsset" }, value: { kind: "Variable", name: { kind: "Name", value: "input" } } }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }] } }] } }], loc: { start: 0, end: 108, source: { body: "\r\nmutation addMediaAsset($input: MediaAssetInput!){\r\n  addMediaAsset(mediaAsset: $input){\r\n    id,\r\n  }\r\n}\r\n", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };

var MarkerAddToVideo = function MarkerAddToVideo() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      orgId = _ref.orgId,
      videoId = _ref.videoId,
      inputs = _ref.inputs,
      client = _ref.client,
      refetchQueries = _ref.refetchQueries,
      rest = (0, _objectWithoutProperties2["default"])(_ref, ["orgId", "videoId", "inputs", "client", "refetchQueries"]);

  return React.createElement(_reactApollo.Mutation, {
    client: client,
    mutation: mutation
  }, function (addMutation) {
    var OnMarkerAdded = function OnMarkerAdded(marker) {
      var markerId = marker.data.addMarker.id;
      addMutation({
        variables: {
          input: {
            orgId: orgId,
            parentVideoId: videoId,
            childMarkerId: markerId,
            metadataKey: 'marker'
          }
        }
      }).then(function () {
        return refetchQueries();
      });
    };

    return React.createElement(_Marker["default"], (0, _extends2["default"])({
      orgId: orgId,
      client: client,
      onCompleted: OnMarkerAdded
    }, rest)); // const onSubmit = (x) => {
    //   let toSub = x;
    //   if (initialValues) {
    //     toSub = Object.assign({}, initialValues, x);
    //   }
    //   const { __typename, ...rest } = toSub;
    //   addMutation({ variables: (translation(rest)) });
    //   handleClose();
    // };
    // const FormToRender = ({ handleSubmit }) => (
    //   <form onSubmit={handleSubmit(onSubmit)}>
    //     { inputs.map((input: any) => (
    //       <FormElement
    //         key={input.id}
    //         {...input}
    //         value={mapValues(initialValues, input)}
    //       />
    //     ))}
    //     {buttons}
    //     {!buttons && (
    //       <div>
    //         {showClose && (
    //           <button type="button" onClick={handleClose} color="primary">
    //             Cancel
    //           </button>
    //         )}
    //         <button type="submit" color="primary" autoFocus>
    //           Submit
    //         </button>
    //       </div>
    //     )}
    //   </form>
    // );
  });
};

var _default = MarkerAddToVideo;
exports["default"] = _default;