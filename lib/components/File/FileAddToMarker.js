"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var React = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _AddToPhotos = _interopRequireDefault(require("@material-ui/icons/AddToPhotos"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var mutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "removeMediaAsset" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "mediaAssetId" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "ID" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "removeMediaAsset" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "mediaAssetId" }, value: { kind: "Variable", name: { kind: "Name", value: "mediaAssetId" } } }], directives: [] }] } }], loc: { start: 0, end: 100, source: { body: "mutation removeMediaAsset($mediaAssetId: ID!){\r\n  removeMediaAsset(mediaAssetId: $mediaAssetId)\r\n}\r\n", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };

var Button = function Button() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      marker = _ref.marker,
      orgId = _ref.orgId;

  return React.createElement(_reactApollo.Mutation, {
    mutation: mutation
  }, function (deleteFunc) {
    return React.createElement("span", null, React.createElement("div", null, React.createElement(_IconButton["default"], {
      color: "primary",
      onClick: function onClick() {
        return deleteFunc({
          variables: {
            markerId: marker.id,
            orgId: orgId
          }
        });
      }
    }, React.createElement(_AddToPhotos["default"], null)), "Add File To Marker"));
  });
};

var _default = Button;
exports["default"] = _default;