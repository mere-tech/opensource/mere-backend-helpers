"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _defineProperty2 = _interopRequireDefault(require("@babel/runtime/helpers/defineProperty"));

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var React = _interopRequireWildcard(require("react"));

var _Add = _interopRequireDefault(require("@material-ui/icons/Add"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var _AddEdit = _interopRequireDefault(require("../AddEdit/AddEdit"));

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(source, true).forEach(function (key) { (0, _defineProperty2["default"])(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(source).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

var mutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "addMediaAsset" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "mediaAsset" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "MediaAssetInput" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "addMediaAsset" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "mediaAsset" }, value: { kind: "Variable", name: { kind: "Name", value: "mediaAsset" } } }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }] } }] } }], loc: { start: 0, end: 116, source: { body: "\r\nmutation addMediaAsset($mediaAsset: MediaAssetInput!){\r\n  addMediaAsset(mediaAsset: $mediaAsset){\r\n    id,\r\n  }\r\n}", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };
var updateMutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "UpdateMarker" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "input" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "MarkerInput" } } }, directives: [] }, { kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "orgId" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "ID" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "updateMarker" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "input" }, value: { kind: "Variable", name: { kind: "Name", value: "input" } } }, { kind: "Argument", name: { kind: "Name", value: "orgId" }, value: { kind: "Variable", name: { kind: "Name", value: "orgId" } } }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "FragmentSpread", name: { kind: "Name", value: "MarkerDetails" }, directives: [] }] } }] } }, { kind: "FragmentDefinition", name: { kind: "Name", value: "MarkerDetails" }, typeCondition: { kind: "NamedType", name: { kind: "Name", value: "Marker" } }, directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "description" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "startTimeSeconds" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "endTimeSeconds" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "childOfAssets" }, arguments: [], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "parentVideo" }, arguments: [], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "FragmentSpread", name: { kind: "Name", value: "VideoDetails" }, directives: [] }] } }] } }, { kind: "Field", name: { kind: "Name", value: "name" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }] } }, { kind: "FragmentDefinition", name: { kind: "Name", value: "VideoDetails" }, typeCondition: { kind: "NamedType", name: { kind: "Name", value: "Video" } }, directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "name" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "smallImage" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "largeImage" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "description" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "displayName" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "hd" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "hls" }, arguments: [], directives: [] }] } }], loc: { start: 0, end: 178, source: { body: "#import \"../../graphql/Marker.graphql\"\r\n\r\nmutation UpdateMarker($input: MarkerInput!, $orgId: ID!){\r\n  updateMarker(input: $input, orgId: $orgId){\r\n    ...MarkerDetails\r\n  }\r\n}\r\n", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };
var inputs = [{
  id: 'metadataKey',
  name: 'Key',
  type: 'string',
  valueKey: 'metadataKey'
}, // {
//   id: 'metadataValue',
//   name: 'Value',
//   type: 'string',
//   valueKey: 'metadataValue',
// },
{
  id: 'childMarkerId',
  name: 'Related To Marker Id',
  type: 'string',
  valueKey: 'childMarkerId'
}, {
  id: 'childFolderId',
  name: 'Related To Folder Id',
  type: 'string',
  valueKey: 'childFolderId'
}, {
  id: 'childVideoId',
  name: 'Related To Video Id',
  type: 'string',
  valueKey: 'childVideoId'
} //   {
//     id: 'downloadUrl',
//     name: 'DownloadUrl',
//     type: 'string',
//     valueKey: 'downloadUrl',
//   },
];

var button = function button() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      mediaAsset = _ref.mediaAsset,
      orgId = _ref.orgId,
      client = _ref.client,
      videoId = _ref.videoId,
      edit = _ref.edit,
      refetchQueries = _ref.refetchQueries;

  return React.createElement(_AddEdit["default"], {
    initialValues: Object.assign({}, mediaAsset, {
      childVideoId: mediaAsset && mediaAsset.childVideo && mediaAsset.childVideo.id,
      childMarkerId: mediaAsset && mediaAsset.childMarker && mediaAsset.childMarker.id,
      childFolderId: mediaAsset && mediaAsset.childFolder && mediaAsset.childFolder.id,
      metadataKey: 'related'
    }),
    inputs: inputs,
    client: client,
    mutation: edit ? updateMutation : mutation // mutation={mutation}
    ,
    refetchQueries: refetchQueries,
    title: edit ? 'Edit Metadata' : 'Add Metadata',
    showClose: true,
    translation: function translation(_ref2) {
      var a = (0, _extends2["default"])({}, _ref2);

      if (edit) {
        return {
          mediaAsset: {
            id: mediaAsset && mediaAsset.id,
            orgId: orgId,
            metadataKey: 'related',
            metadataValue: a.metadataValue,
            downloadUrl: a.downloadUrl,
            parentVideoId: videoId,
            childVideoId: a.childVideoId,
            childMarkerId: a.childMarkerId,
            childFolderId: a.childFolderId
          }
        };
      }

      return {
        mediaAsset: _objectSpread({
          orgId: orgId,
          parentVideoId: videoId
        }, a)
      };
    },
    trigger: function trigger(showDialog) {
      return React.createElement("div", null, React.createElement(_IconButton["default"], {
        color: "primary",
        onClick: showDialog
      }, React.createElement(_Add["default"], null)), "Add Related");
    }
  });
};

var _default = button;
exports["default"] = _default;