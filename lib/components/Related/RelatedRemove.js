"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var React = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _Delete = _interopRequireDefault(require("@material-ui/icons/Delete"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var mutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "removeMediaAsset" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "mediaAssetId" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "ID" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "removeMediaAsset" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "mediaAssetId" }, value: { kind: "Variable", name: { kind: "Name", value: "mediaAssetId" } } }], directives: [] }] } }], loc: { start: 0, end: 100, source: { body: "mutation removeMediaAsset($mediaAssetId: ID!){\r\n  removeMediaAsset(mediaAssetId: $mediaAssetId)\r\n}\r\n", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };

var button = function button() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      mediaAssetId = _ref.mediaAssetId,
      refetchQueries = _ref.refetchQueries,
      client = _ref.client;

  return React.createElement(_reactApollo.Mutation, {
    client: client,
    mutation: mutation
  }, function (removeMediaAsset) {
    return React.createElement("span", null, React.createElement(_IconButton["default"], {
      color: "primary",
      onClick: function onClick() {
        return removeMediaAsset({
          variables: {
            mediaAssetId: mediaAssetId
          },
          refetchQueries: refetchQueries
        });
      }
    }, React.createElement(_Delete["default"], null)));
  });
};

var _default = button;
exports["default"] = _default;