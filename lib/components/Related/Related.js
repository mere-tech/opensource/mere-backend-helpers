"use strict";

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var _objectWithoutProperties2 = _interopRequireDefault(require("@babel/runtime/helpers/objectWithoutProperties"));

var React = _interopRequireWildcard(require("react"));

var _Add = _interopRequireDefault(require("@material-ui/icons/Add"));

var _Edit = _interopRequireDefault(require("@material-ui/icons/Edit"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var _AddEdit = _interopRequireDefault(require("../AddEdit/AddEdit"));

var mutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "addMediaAsset" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "mediaAsset" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "MediaAssetInput" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "addMediaAsset" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "mediaAsset" }, value: { kind: "Variable", name: { kind: "Name", value: "mediaAsset" } } }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }] } }] } }], loc: { start: 0, end: 116, source: { body: "\r\nmutation addMediaAsset($mediaAsset: MediaAssetInput!){\r\n  addMediaAsset(mediaAsset: $mediaAsset){\r\n    id,\r\n  }\r\n}", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };
var updateMutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "UpdateMarker" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "input" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "MarkerInput" } } }, directives: [] }, { kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "orgId" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "ID" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "updateMarker" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "input" }, value: { kind: "Variable", name: { kind: "Name", value: "input" } } }, { kind: "Argument", name: { kind: "Name", value: "orgId" }, value: { kind: "Variable", name: { kind: "Name", value: "orgId" } } }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "FragmentSpread", name: { kind: "Name", value: "MarkerDetails" }, directives: [] }] } }] } }, { kind: "FragmentDefinition", name: { kind: "Name", value: "MarkerDetails" }, typeCondition: { kind: "NamedType", name: { kind: "Name", value: "Marker" } }, directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "description" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "startTimeSeconds" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "endTimeSeconds" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "childOfAssets" }, arguments: [], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "parentVideo" }, arguments: [], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "FragmentSpread", name: { kind: "Name", value: "VideoDetails" }, directives: [] }] } }] } }, { kind: "Field", name: { kind: "Name", value: "name" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }] } }, { kind: "FragmentDefinition", name: { kind: "Name", value: "VideoDetails" }, typeCondition: { kind: "NamedType", name: { kind: "Name", value: "Video" } }, directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "name" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "smallImage" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "largeImage" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "description" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "displayName" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "hd" }, arguments: [], directives: [] }, { kind: "Field", name: { kind: "Name", value: "hls" }, arguments: [], directives: [] }] } }], loc: { start: 0, end: 178, source: { body: "#import \"../../graphql/Marker.graphql\"\r\n\r\nmutation UpdateMarker($input: MarkerInput!, $orgId: ID!){\r\n  updateMarker(input: $input, orgId: $orgId){\r\n    ...MarkerDetails\r\n  }\r\n}\r\n", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };
var inputs = [{
  id: 'name',
  name: 'Name',
  type: 'string',
  valueKey: 'name'
}, {
  id: 'description',
  name: 'Description',
  type: 'string',
  valueKey: 'description'
}, {
  id: 'startTimeSeconds',
  name: 'Start Time In Seconds',
  type: 'string',
  valueKey: 'startTimeSeconds'
}, {
  id: 'endTimeSeconds',
  name: 'End Time In Seconds',
  type: 'string',
  valueKey: 'endTimeSeconds'
}];

var Button = function Button() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      orgId = _ref.orgId,
      edit = _ref.edit,
      related = _ref.related,
      props = (0, _objectWithoutProperties2["default"])(_ref, ["orgId", "edit", "related"]);

  return React.createElement(_AddEdit["default"], (0, _extends2["default"])({}, props, {
    initialValues: related,
    inputs: inputs,
    mutation: edit ? updateMutation : mutation,
    title: edit ? 'Edit Related' : 'Add Related',
    showClose: true,
    translation: function translation(_ref2) {
      var id = _ref2.id,
          description = _ref2.description,
          endTimeSeconds = _ref2.endTimeSeconds,
          startTimeSeconds = _ref2.startTimeSeconds,
          name = _ref2.name;
      return {
        orgId: orgId,
        input: {
          id: id,
          description: description,
          endTimeSeconds: endTimeSeconds,
          startTimeSeconds: startTimeSeconds,
          name: name
        }
      };
    },
    trigger: function trigger(showDialog) {
      return React.createElement("div", null, React.createElement(_IconButton["default"], {
        color: "primary",
        onClick: showDialog
      }, edit ? React.createElement(_Edit["default"], null) : React.createElement(_Add["default"], null)), edit ? 'Edit Related' : 'Add Related');
    }
  }));
};

var _default = Button;
exports["default"] = _default;