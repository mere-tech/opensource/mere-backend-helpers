"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var React = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _Delete = _interopRequireDefault(require("@material-ui/icons/Delete"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var mutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "DeleteATag" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "orgId" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "ID" } } }, directives: [] }, { kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "tagId" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "ID" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "deleteATag" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "orgId" }, value: { kind: "Variable", name: { kind: "Name", value: "orgId" } } }, { kind: "Argument", name: { kind: "Name", value: "tagId" }, value: { kind: "Variable", name: { kind: "Name", value: "tagId" } } }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }] } }] } }], loc: { start: 0, end: 111, source: { body: "\r\nmutation DeleteATag($orgId: ID!, $tagId: ID!){\r\n  deleteATag(orgId: $orgId, tagId: $tagId){\r\n    id\r\n  }\r\n}\r\n", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };

var Button = function Button() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      tag = _ref.tag,
      orgId = _ref.orgId;

  return React.createElement(_reactApollo.Mutation, {
    mutation: mutation
  }, function (deleteFunc) {
    return React.createElement("span", null, React.createElement("div", null, React.createElement(_IconButton["default"], {
      color: "primary",
      onClick: function onClick() {
        return deleteFunc({
          variables: {
            tagId: tag.id,
            orgId: orgId
          }
        });
      }
    }, React.createElement(_Delete["default"], null)), "Remove Tag"));
  });
};

var _default = Button;
exports["default"] = _default;