"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

var _interopRequireWildcard = require("@babel/runtime/helpers/interopRequireWildcard");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var React = _interopRequireWildcard(require("react"));

var _reactApollo = require("react-apollo");

var _Delete = _interopRequireDefault(require("@material-ui/icons/Delete"));

var _IconButton = _interopRequireDefault(require("@material-ui/core/IconButton"));

var mutation = { kind: "Document", definitions: [{ kind: "OperationDefinition", operation: "mutation", name: { kind: "Name", value: "UnlinkTag" }, variableDefinitions: [{ kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "orgId" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "ID" } } }, directives: [] }, { kind: "VariableDefinition", variable: { kind: "Variable", name: { kind: "Name", value: "tagLinkInput" } }, type: { kind: "NonNullType", type: { kind: "NamedType", name: { kind: "Name", value: "TagLinkInput" } } }, directives: [] }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "unlinkTag" }, arguments: [{ kind: "Argument", name: { kind: "Name", value: "orgId" }, value: { kind: "Variable", name: { kind: "Name", value: "orgId" } } }, { kind: "Argument", name: { kind: "Name", value: "tagLinkInput" }, value: { kind: "Variable", name: { kind: "Name", value: "tagLinkInput" } } }], directives: [], selectionSet: { kind: "SelectionSet", selections: [{ kind: "Field", name: { kind: "Name", value: "id" }, arguments: [], directives: [] }] } }] } }], loc: { start: 0, end: 140, source: { body: "\r\nmutation UnlinkTag($orgId: ID!, $tagLinkInput: TagLinkInput!){\r\n  unlinkTag(orgId: $orgId, tagLinkInput: $tagLinkInput){\r\n    id\r\n  }\r\n}\r\n", name: "GraphQL request", locationOffset: { line: 1, column: 1 } } } };

var TagUnlink = function TagUnlink() {
  var _ref = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {},
      orgId = _ref.orgId,
      markerId = _ref.markerId,
      name = _ref.name,
      client = _ref.client;

  return React.createElement(_reactApollo.Mutation, {
    client: client,
    mutation: mutation
  }, function (deleteFunc) {
    return React.createElement(_IconButton["default"], {
      color: "primary",
      onClick: function onClick() {
        return deleteFunc({
          variables: {
            orgId: orgId,
            tagLinkInput: {
              name: name,
              markerId: markerId
            }
          }
        });
      }
    }, React.createElement(_Delete["default"], null));
  });
};

var _default = TagUnlink;
exports["default"] = _default;