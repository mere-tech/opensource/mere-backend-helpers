/* @flow */
import * as React from 'react';
import { Mutation } from 'react-apollo';
import { reduxForm } from 'redux-form';
import type { DocumentNode } from 'graphql';
import FormElement from './FormElement';
import type { FormElementProp } from './FormElement';

export type EditFormInput = {
  mutation: DocumentNode,
  translation: (any) => any,
  translateFilter: (any) => any,
  handleClose: () => void,
  onCompleted: (any) => any,
  client: any,
  showClose?: boolean,
  inputs: Array<FormElementProp>,
  buttons?: any,
  initialValues?: any,
  editType: string,
  refetchQueries?: (any) => Array<{ query: DocumentNode, variables?: any}>,
};

const mapValues = (initialValues, field: FormElementProp) => {
  if (initialValues && initialValues[field.valueKey]) {
    return initialValues[field.valueKey];
  }
  return '';
};

const Form = ({
  inputs,
  mutation,
  translation,
  translateFilter,
  editType,
  handleClose,
  buttons,
  showClose,
  refetchQueries,
  initialValues,
  onCompleted,
  client,
  success,
}: EditFormInput = {}) => {
  const onSubmit = (x) => {
    let toSub = x;

    if (initialValues) {
      toSub = Object.assign({}, initialValues, x);
    }
    const { __typename, ...rest } = toSub;

    if (rest.filter !== undefined) {
      rest.name = `${rest.filter} ${rest.name}`;
    }

    const mutationParam = {
      mutation,
      variables: translation(rest),
    };

    const thisType = { editType };
    if (thisType.editType === 'editTags') {
      client.mutate(mutationParam).then((response) => {
        if (rest.filter !== undefined && rest.filter !== 'General:') {
          const newRest = translateFilter(rest);
          newRest.tagLinkInput.name = 'Filters';
          newRest.tagLinkInput.childTagId = response.data.addTagTo.tagId;

          client.mutate({ mutation, variables: newRest }).then(handleClose());
        } else {
          handleClose();
        }
      });
    } else {
      client.mutate(mutationParam).then((onCompleted));
      handleClose();
    }
    
    // client.mutate(mutationParam).then((response) => {
    //   const thisType = { editType };
    //   if (thisType.editType === 'editTags') {
    //     if (rest.filter !== undefined && rest.filter !== 'General:') {
    //       const newRest = translateFilter(rest);
    //       newRest.tagLinkInput.name = 'Filters';
    //       newRest.tagLinkInput.childTagId = response.data.addTagTo.tagId;

    //       client.mutate({ mutation, variables: newRest }).then(handleClose());
    //     } else {
    //       handleClose();
    //     }
    //   } else {
    //     handleClose();
    //   }
    // });
  };

  const FormToRender = ({ handleSubmit }) => (
    <form onSubmit={handleSubmit(onSubmit)}>
      { inputs.map((input: any) => (
        <FormElement
          key={input.id}
          {...input}
          value={mapValues(initialValues, input)}
        />
      ))}
      {buttons}
      {!buttons && (
        <div>
          {showClose && (
          <button type="button" onClick={handleClose} color="primary">
                  Cancel
          </button>
          )}
          <button type="submit" color="primary" autoFocus>
                Submit
          </button>
        </div>
      )}
    </form>
  );

  const RedForm = reduxForm({
    form: `add${Date.now()}`,
    initialValues,
    destroyOnUnmount: false,
    enableReinitialize: true,
  })(FormToRender);

  return (<RedForm />);
};

export default Form;
