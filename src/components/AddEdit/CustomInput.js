/* eslint-disable react/prop-types */
import React from 'react';
// @material-ui/core components
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';

function CustomInput({ ...props }) {
  const {
    formControlProps,
    labelText,
    id,
    labelProps,
    inputProps,
    helpText,
  } = props;

  return (
    <FormControl {...formControlProps}>
      {labelText !== undefined ? (
        <InputLabel
          htmlFor={id}
          {...labelProps}
        >
          {labelText}
        </InputLabel>
      ) : null}
      <Input
        id={id}
        {...inputProps}
      />
      {helpText !== undefined ? (
        <FormHelperText id={`${id}-text`}>
          {helpText}
        </FormHelperText>
      ) : null}
    </FormControl>
  );
}

export default CustomInput;
