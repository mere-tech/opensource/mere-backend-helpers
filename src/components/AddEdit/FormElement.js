/* @flow */
import * as React from 'react';
import { Field } from 'redux-form';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import InputLabel from '@material-ui/core/InputLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Check from '@material-ui/icons/Check';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import CustomInput from './CustomInput';

const filterChange = event => (
  // eslint-disable-next-line no-console
  console.log(event.target.value)
);

const renderCheckbox = ({
  input,
  placeholder,
  classes,
  defaultValue,
}: any): React.Node => (
  <div className={classes.checkboxAndRadio}>
    <FormControlLabel
      control={(
        <Checkbox
          inputProps={{ ...input }}
          checked={input.value !== '' ? input.value : defaultValue}
          value={input.value}
          tabIndex={-1}
          onClick={param => input.onChange(param)}
          checkedIcon={<Check className={classes.checkedIcon} />}
          icon={<Check className={classes.uncheckedIcon} />}
          classes={{
            checked: classes.checked,
          }}
        />
      )}
      label={placeholder}
    />
  </div>
);

const renderSelectField = ({
  input,
  label,
  meta: { touched, error },
  children,
  ...custom
}) => (
  <FormControl error={touched && error}>
    <InputLabel htmlFor="tag-type-native-simple">Tag Type:</InputLabel>
    <Select
      native
      {...input}
      {...custom}
      inputProps={{
        name: 'tag-type',
        id: 'tag-type-native-simple',
      }}
    >
      {children}
    </Select>
  </FormControl>
);

// const renderSelect = ({ placeholder, input, defaultValue, value }: any): React.Node => (
//   <Select
//     labelId="demo-simple-select-label"
//     id="demo-simple-select"
//     value={defaultValue}
//     onChange={(param) => {
//       input.onChange(console.log(param));
//     }}
//     a={console.log(value)}
//     b={console.log(placeholder)}
//     c={console.log(input)}
//     d={console.log(defaultValue)}
//   >
//     <MenuItem value="General:">General</MenuItem>
//     <MenuItem value="Type:">Type</MenuItem>
//     <MenuItem value="Author:">Author</MenuItem>
//     <MenuItem value="Application:">Application</MenuItem>
//     <MenuItem value="Discipline:">Discipline</MenuItem>
//   </Select>
// );

const renderField = ({ placeholder, input, defaultValue, value }: any): React.Node => (
  <CustomInput
    labelText={placeholder}
    inputProps={{
      value,
      onChange: param => input.onChange(param),
      defaultValue,
    }}
    formControlProps={{
      fullWidth: true,
    }}
  />
);

const renderIntegerField = ({ placeholder, input }: any): React.Node => (
  <CustomInput
    labelText={placeholder}
    inputProps={{
      value: input.value,
      onChange: param => input.onChange(param),
    }}
    formControlProps={{
      fullWidth: true,
    }}
  />
);

type formType =
  "string" |
  "select" |
  "email" |
  "boolean" |
  "integer" |
  "na";

export type FormElementProp = {
  // id: string,
  name: string,
  type: formType,
  valueKey: string,
  value?: string,
  // defaultProp: string,
};
const RenderFormElement = ({
  name,
  type,
  valueKey,
  value,
}: FormElementProp): React.Node => {
  switch (type) {
    case 'select':
      return (
        <Field
          label="Favorite Color"
          name={valueKey}
          type="select"
          component={renderSelectField}
          defaultValue={value}
          placeholder={name}
        >
          <option value=""> </option>
          <option value="Action:">Action:</option>
          <option value="Application:">Application:</option>
          <option value="Author:">Author:</option>
          <option value="Command:">Command:</option>
          <option value="Course:">Course:</option>
          <option value="General:">General:</option>
          <option value="Discipline:">Discipline:</option>
          <option value="Program:">Program:</option>
          <option value="Tool:">Tool:</option>
          <option value="Type:">Type:</option>
        </Field>
      );
    case 'string':
      return (
        <Field
          name={valueKey}
          type="text"
          component={renderField}
          placeholder={name}
          defaultValue={value}
        />
      );
    case 'integer':
      return (
        <Field
          name={valueKey}
          type="number"
          format={val => (val ? val.toString() : '0')}
          parse={val => (Number.isNaN(parseInt(val, 10)) ? null : parseInt(val, 10))}
          component={renderIntegerField}
          placeholder={name}
          defaultValue={value}
        />
      );
    case 'boolean':
      return (
        <Field
          type="text"
          name={valueKey}
          component={renderCheckbox}
          placeholder={name}
          defaultValue={value}
        />
      );
      // renderField();
      // return (
      //   <GridContainer>
      //     <ItemGrid xs={12} sm={12} md={3}>
      //       <FormLabel>
      //         {name}
      //       </FormLabel>
      //     </ItemGrid>
      //     <ItemGrid xs={12} sm={12} md={9}>
      //       <CustomInput
      //         id={id}
      //         value={defaultProp}
      //         formControlProps={{
      //           fullWidth: true,
      //         }}
      //       />
      //     </ItemGrid>
      //   </GridContainer>
      // );
    default:
      return null;
  }
};

export default RenderFormElement;
