/* @flow */
/*eslint-disable */
import * as React from 'react';
import Drawer from '@material-ui/core/Drawer';
import Form from './Form';
import { withStyles } from '@material-ui/core/styles';

const styles = {
  paperAnchorRight: {
    padding: 100,
    minWidth: 650,
    maxWidth: 1000,
  },
};
  
export type EditDialogInput = {
  editType: string,
  mutation: DocumentNode,
  header: any,
  trigger: (() => void) => React.Node,
  title: string,
  translation: (any) => any,
  translateFilter: (any) => any,
  onClose: () => any,
  onCompleted: (any) => any,
  classes: any,
  inputs: Array<FormElementProp>,
};
  
class SimpleDialog extends React.Component<EditDialogInput, State> {
  state = {
    showDialog: false,
  };

  showDialogCallback = () => {
    this.setState({ showDialog: true });
  }

  handleClose = () => {
    const { onClose } = this.props;
    if (onClose) {
      onClose();
    }
    this.setState({ showDialog: false });
  };
  
  render() {
    const {
      title,
      editType,
      trigger,
      header,
    } = this.props;
    const { showDialog } = this.state;
    return (
      <span>
        {trigger(this.showDialogCallback)}
        { showDialog && (
          <Drawer classes={this.props.classes} anchor="right" open={showDialog}>
            <h4>{title}</h4>
            {header}
            <Form
              {...this.props}
              showClose
              handleClose={this.handleClose}
            />
          </Drawer>
        )}
      </span>
    );
  }
}
  
export default withStyles(styles)(SimpleDialog);