/* @flow */
import * as React from 'react';
import Add from '@material-ui/icons/Add';
import Edit from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import AddDialog from '../AddEdit/AddEdit';
import mutation from './Add.graphql';
import updateMutation from './Update.graphql';

const inputs: Array<FormElementProp> = [
  {
    id: 'name',
    name: 'Name',
    type: 'string',
    valueKey: 'name',
  },
  {
    id: 'description',
    name: 'Description',
    type: 'string',
    valueKey: 'description',
  },
  {
    id: 'startTimeSeconds',
    name: 'Start Time In Seconds',
    type: 'string',
    valueKey: 'startTimeSeconds',
  },
  {
    id: 'endTimeSeconds',
    name: 'End Time In Seconds',
    type: 'string',
    valueKey: 'endTimeSeconds',
  },
];

type Prop = {
  orgId: any,
  edit?: boolean,
  related?: MarkerDetails,
};

const Button = ({
  orgId,
  edit,
  related,
  ...props
}: Prop = {}) => (
  <AddDialog
    {...props}
    initialValues={related}
    inputs={inputs}
    mutation={edit ? updateMutation : mutation}
    title={edit ? 'Edit Related' : 'Add Related'}
    showClose
    translation={({
      id, description, endTimeSeconds, startTimeSeconds, name,
    }) => (
      {
        orgId,
        input: {
          id, description, endTimeSeconds, startTimeSeconds, name,
        },
      }
    )}
    trigger={showDialog => (
      <div>
        <IconButton
          color="primary"
          onClick={showDialog}
        >
          {edit ? (
            <Edit />
          ) : (
            <Add />
          )}
        </IconButton>
        {edit ? (
          'Edit Related'
        ) : (
          'Add Related'
        )}
      </div>
    )}
  />
);

export default Button;
