/* @flow */
import * as React from 'react';
import Add from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import AddDialog from '../AddEdit/AddEdit';
import mutation from './Add.graphql';
import updateMutation from './Update.graphql';

const inputs: Array<FormElementProp> = [
  {
    id: 'metadataKey',
    name: 'Key',
    type: 'string',
    valueKey: 'metadataKey',
  },
  // {
  //   id: 'metadataValue',
  //   name: 'Value',
  //   type: 'string',
  //   valueKey: 'metadataValue',
  // },
  {
    id: 'childMarkerId',
    name: 'Related To Marker Id',
    type: 'string',
    valueKey: 'childMarkerId',
  },
  {
    id: 'childFolderId',
    name: 'Related To Folder Id',
    type: 'string',
    valueKey: 'childFolderId',
  },
  {
    id: 'childVideoId',
    name: 'Related To Video Id',
    type: 'string',
    valueKey: 'childVideoId',
  },
//   {
//     id: 'downloadUrl',
//     name: 'DownloadUrl',
//     type: 'string',
//     valueKey: 'downloadUrl',
//   },
];

type Prop = {
  edit?: boolean,
  orgId: any,
  videoId: any,
  client: client,
  mediaAsset?: MediaAssetDetails,
  refetchQueries: any,
};

const button = ({
  mediaAsset,
  orgId,
  client,
  videoId,
  edit,
  refetchQueries,
}: Prop = {}) => (
  <AddDialog
    initialValues={Object.assign({}, mediaAsset, {
      childVideoId: mediaAsset && mediaAsset.childVideo && mediaAsset.childVideo.id,
      childMarkerId: mediaAsset && mediaAsset.childMarker && mediaAsset.childMarker.id,
      childFolderId: mediaAsset && mediaAsset.childFolder && mediaAsset.childFolder.id,
      metadataKey: 'related',
    })}
    inputs={inputs}
    client={client}
    mutation={edit ? updateMutation : mutation}
    // mutation={mutation}
    refetchQueries={refetchQueries}
    title={edit ? 'Edit Metadata' : 'Add Metadata'}
    showClose
    translation={({ ...a }) => {
      if (edit) {
        return {
          mediaAsset: {
            id: mediaAsset && mediaAsset.id,
            orgId,
            metadataKey: 'related',
            metadataValue: a.metadataValue,
            downloadUrl: a.downloadUrl,
            parentVideoId: videoId,
            childVideoId: a.childVideoId,
            childMarkerId: a.childMarkerId,
            childFolderId: a.childFolderId,
          },
        };
      }
      return ({ mediaAsset: { orgId, parentVideoId: videoId, ...a } });
    }}
    trigger={showDialog => (
      <div>
        <IconButton
          color="primary"
          onClick={showDialog}
        >
          <Add />
        </IconButton>
        Add Related
      </div>
    )}
  />
);

export default button;
