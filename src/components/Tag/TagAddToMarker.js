/* @flow */
import * as React from 'react';
import Add from '@material-ui/icons/Add';
import Edit from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import AddDialog from '../AddEdit/AddEdit';
import mutation from './Add.graphql';
import updateMutation from './Update.graphql';

const inputs: Array<FormElementProp> = [
  {
    id: 'name',
    name: 'Name',
    type: 'string',
    valueKey: 'name',
  },
  {
    id: 'filter',
    name: 'Filter Type',
    type: 'select',
    valueKey: 'filter',
  },
];

type Prop = {
  orgId: any,
  markerId: any,
  childTagId: any,
  edit?: boolean,
  tag?: TagDetails,
};

const AddTagToMarker = ({
  orgId,
  markerId,
  childTagId,
  edit,
  tag,
  ...props
}: Prop = {}) => (
  <AddDialog
    {...props}
    editType="editTags"
    initialValues={tag}
    inputs={inputs}
    mutation={edit ? updateMutation : mutation}
    title={edit ? 'Edit Tag' : 'Add Tag To Marker'}
    showClose
    translation={({
      name,
    }) => (
      {
        orgId,
        tagLinkInput: {
          name,
          markerId,
        },
      }
    )}
    translateFilter={({
      name,
    }) => (
      {
        orgId,
        tagLinkInput: {
          name,
          childTagId,
        },
      }
    )}
    // success={linkFilters => (
    //   newRest = translateFilter(rest);
    //   newRest.tagLinkInput.name = 'Filters';
    //   newRest.tagLinkInput.childTagId = response.data.addTagTo.tagId;

    //   client.mutate({ mutation, variables: newRest }).then(handleClose());
    // )}
    trigger={showDialog => (
      <div>
        <IconButton
          color="primary"
          onClick={showDialog}
        >
          {edit ? (
            <Edit />
          ) : (
            <Add />
          )}
        </IconButton>
        {edit ? (
          'Edit Tag'
        ) : (
          'Add Tag To Marker'
        )}
      </div>
    )}
  />
);

export default AddTagToMarker;
