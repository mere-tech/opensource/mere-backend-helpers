/* @flow */
import * as React from 'react';
import { Mutation } from 'react-apollo';
import Delete from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import mutation from './Delete.graphql';


type Prop = {
  orgId: any,
  tag: TagDetails,
};

const Button = ({ tag, orgId }: Prop = {}) => (
  <Mutation mutation={mutation}>
    {deleteFunc => (
      <span>
        <div>
          <IconButton
            color="primary"
            onClick={() => deleteFunc({
              variables: {
                tagId: tag.id,
                orgId,
              },
            })}
          >
            <Delete />
          </IconButton>
          Remove Tag
        </div>
      </span>
    )}
  </Mutation>
);

export default Button;
