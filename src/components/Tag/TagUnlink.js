/* @flow */
import * as React from 'react';
import { Mutation } from 'react-apollo';
import Delete from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import mutation from './Unlink.graphql';

type Prop = {
  orgId: any,
  videoId: any,
  name: any,
  client: client,
};

const TagUnlink = ({ orgId, videoId, name, client }: Prop = {}) => (
  <Mutation 
    client={client}
    mutation={mutation}
  >
    {deleteFunc => (
      <IconButton
        color="primary"
        onClick={() => deleteFunc({
          variables: {
            orgId,
            tagLinkInput: {
              name,
              videoId,
            },
          },
        })}
      >
        <Delete />
      </IconButton>
    )}
  </Mutation>
);

export default TagUnlink;
