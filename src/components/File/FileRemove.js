/* @flow */
import * as React from 'react';
import { Mutation } from 'react-apollo';
import Delete from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import mutation from './Delete.graphql';


type Prop = {
  mediaAssetId: any,
  refetchQueries: any,
  client: client,
};

const button = ({ mediaAssetId, refetchQueries, client }: Prop = {}) => (
  <Mutation
    client={client}
    mutation={mutation}
    refetchQueries={refetchQueries}
  >
    {removeMediaAsset => (
      <span>
        <IconButton
          color="primary"
          onClick={() => removeMediaAsset({
            variables: {
              mediaAssetId,
            },
            refetchQueries,
          })}
        >
          <Delete />
        </IconButton>
      </span>
    )}
  </Mutation>
);

export default button;
