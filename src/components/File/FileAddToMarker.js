/* @flow */
import * as React from 'react';
import { Mutation } from 'react-apollo';
import AddToMarker from '@material-ui/icons/AddToPhotos';
import IconButton from '@material-ui/core/IconButton';
import mutation from './Delete.graphql';


type Prop = {
  orgId: any,
  marker: MarkerDetails,
};

const Button = ({ marker, orgId }: Prop = {}) => (
  <Mutation mutation={mutation}>
    {deleteFunc => (
      <span>
        <div>
          <IconButton
            color="primary"
            onClick={() => deleteFunc({
              variables: {
                markerId: marker.id,
                orgId,
              },
            })}
          >
            <AddToMarker />
          </IconButton>
          Add File To Marker
        </div>
      </span>
    )}
  </Mutation>
);

export default Button;
