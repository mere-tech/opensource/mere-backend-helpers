
/* @flow */
import * as React from 'react';
import Add from '@material-ui/icons/Add';
import IconButton from '@material-ui/core/IconButton';
import AddDialog from '../AddEdit/AddEdit';
import mutation from './Add.graphql';
import updateMutation from './Update.graphql';

const inputs: Array<FormElementProp> = [
  {
    id: 'metadataKey',
    name: 'Key',
    type: 'string',
    valueKey: 'metadataKey',
  },
  {
    id: 'childMarkerId',
    name: 'Marker Id (Optional)',
    type: 'string',
    valueKey: 'childMarkerId',
  },
  {
    id: 'questionId',
    name: 'Question',
    type: 'string',
    valueKey: 'questionId',
  },
  {
    id: 'answerId',
    name: 'Answer',
    type: 'string',
    valueKey: 'answerId',
  },
];

type Prop = {
  edit?: boolean,
  orgId: any,
  videoId: any,
  client: client,
  mediaAsset?: MediaAssetDetails,
  refetchQueries: any,
};

const button = ({
  mediaAsset,
  orgId,
  videoId,
  client,
  edit,
  refetchQueries,
}: Prop = {}) => (
  <AddDialog
    initialValues={Object.assign({}, mediaAsset, {
      childVideoId: mediaAsset && mediaAsset.childVideo && mediaAsset.childVideo.id,
      childMarkerId: mediaAsset && mediaAsset.childMarker && mediaAsset.childMarker.id,
      childFolderId: mediaAsset && mediaAsset.childFolder && mediaAsset.childFolder.id,
      metadataKey: 'faq',
    })}
    inputs={inputs}
    client={client}
    mutation={edit ? updateMutation : mutation}
    // mutation={mutation}
    refetchQueries={refetchQueries}
    title={edit ? 'Edit Metadata' : 'Add Metadata'}
    showClose
    translation={({ ...a }) => {
      if (edit) {
        return {
          mediaAsset: {
            id: mediaAsset && mediaAsset.id,
            orgId,
            metadataKey: 'faq',
            metadataValue: '{"question":"this is the question","answer":"this is the answer"}',
            downloadUrl: a.downloadUrl,
            parentVideoId: videoId,
            childVideoId: a.childVideoId,
            childMarkerId: a.childMarkerId,
            childFolderId: a.childFolderId,
          },
        };
      }
      return ({
        mediaAsset: {
          orgId,
          parentVideoId: videoId,
          metadataKey: a.metadataKey,
          metadataValue: `{"question":"${a.questionId}","answer":"${a.answerId}"}`,
        },
      });
    }}
    trigger={showDialog => (
      <div>
        <IconButton
          color="primary"
          onClick={showDialog}
        >
          <Add />
        </IconButton>
        Add Faq
      </div>
    )}
  />
);

export default button;
