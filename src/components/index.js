import AddEdit from './AddEdit/AddEdit';
import Marker from './Marker/Marker';
import MarkerAddToVideo from './Marker/MarkerAddToVideo';
import MarkerRemove from './Marker/MarkerRemove';
import Tag from './Tag/Tag';
import TagAddToMarker from './Tag/TagAddToMarker';
import TagRemove from './Tag/TagRemove';
import TagUnlink from './Tag/TagUnlink';
import TagUnlinkMarker from './Tag/TagUnlinkMarker';
import File from './File/File';
import FileRemove from './File/FileRemove';
import FileAddToVideo from './File/FileAddToVideo';
import FileAddToMarker from './File/FileAddToMarker';
import Faq from './Faq/Faq';
import FaqAddToVideo from './Faq/FaqAddToVideo';
import FaqRemove from './Faq/FaqRemove';
import Related from './Related/Related';
import RelatedRemove from './Related/RelatedRemove';
import RelatedAddToVideo from './Related/RelatedAddToVideo';

export {
  AddEdit,
  Marker,
  MarkerAddToVideo,
  MarkerRemove,
  Tag,
  TagRemove,
  TagUnlink,
  TagUnlinkMarker,
  TagAddToMarker,
  File,
  FileRemove,
  FileAddToVideo,
  FileAddToMarker,
  Faq,
  FaqAddToVideo,
  FaqRemove,
  Related,
  RelatedRemove,
  RelatedAddToVideo,
};
