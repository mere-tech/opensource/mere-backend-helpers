/* @flow */
import * as React from 'react';
import { Mutation } from 'react-apollo';
import Delete from '@material-ui/icons/Delete';
import IconButton from '@material-ui/core/IconButton';
import mutation from './Delete.graphql';


type Prop = {
  mediaAsset: any,
  refetchQueries: any,
  client: client,
};

const button = ({ mediaAsset, refetchQueries, client }: Prop = {}) => (
  <Mutation
    client={client}
    mutation={mutation}
    refetchQueries={refetchQueries}
  >
    {removeMediaAsset => (
      <span>
        <IconButton
          color="primary"
          onClick={() => removeMediaAsset({
            variables: {
              mediaAssetId: mediaAsset.id,
            },
            refetchQueries,
          })}
        >
          <Delete />
        </IconButton>
        Remove Marker
      </span>
    )}
  </Mutation>
);

export default button;
