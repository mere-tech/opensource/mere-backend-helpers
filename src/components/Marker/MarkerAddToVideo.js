/* @flow */
import * as React from 'react';
import { Mutation } from 'react-apollo';
import Marker from './Marker';
import mutation from './AddToVideo.graphql';

type Prop = {
  orgId: any,
  videoId: string,
  inputs: any,
  client: client,
  refetchQueries: any,
};

const MarkerAddToVideo = ({
  orgId,
  videoId,
  inputs,
  client,
  refetchQueries,
  ...rest
}: Prop = {}) => (
  <Mutation client={client} mutation={mutation}>
    {(addMutation) => {
      const OnMarkerAdded = (marker) => {
        const markerId = marker.data.addMarker.id;
        addMutation({
          variables: {
            input: {
              orgId,
              parentVideoId: videoId,
              childMarkerId: markerId,
              metadataKey: 'marker',
            },
          },
        }).then(() => refetchQueries());
      };

      return <Marker orgId={orgId} client={client} onCompleted={OnMarkerAdded} {...rest} />;
      // const onSubmit = (x) => {
      //   let toSub = x;
      //   if (initialValues) {
      //     toSub = Object.assign({}, initialValues, x);
      //   }
      //   const { __typename, ...rest } = toSub;
      //   addMutation({ variables: (translation(rest)) });
      //   handleClose();
      // };
      // const FormToRender = ({ handleSubmit }) => (
      //   <form onSubmit={handleSubmit(onSubmit)}>
      //     { inputs.map((input: any) => (
      //       <FormElement
      //         key={input.id}
      //         {...input}
      //         value={mapValues(initialValues, input)}
      //       />
      //     ))}
      //     {buttons}
      //     {!buttons && (
      //       <div>
      //         {showClose && (
      //           <button type="button" onClick={handleClose} color="primary">
      //             Cancel
      //           </button>
      //         )}
      //         <button type="submit" color="primary" autoFocus>
      //           Submit
      //         </button>
      //       </div>
      //     )}
      //   </form>
      // );
    }}
  </Mutation>
);

export default MarkerAddToVideo;
