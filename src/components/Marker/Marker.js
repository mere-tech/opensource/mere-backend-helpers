/* @flow */
import * as React from 'react';
import Add from '@material-ui/icons/Add';
import Edit from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import AddDialog from '../AddEdit/AddEdit';
import mutation from './Add.graphql';
import updateMutation from './Update.graphql';

const inputs: Array<FormElementProp> = [
  {
    id: 'name',
    name: 'Name',
    type: 'string',
    valueKey: 'name',
  },
  {
    id: 'description',
    name: 'Description',
    type: 'string',
    valueKey: 'description',
  },
  {
    id: 'startTimeSeconds',
    name: 'Start Time In Seconds',
    type: 'string',
    valueKey: 'startTimeSeconds',
  },
  {
    id: 'endTimeSeconds',
    name: 'End Time In Seconds',
    type: 'string',
    valueKey: 'endTimeSeconds',
  },
];

type Prop = {
  orgId: any,
  edit?: boolean,
  currentTime?: number,
  marker?: MarkerDetails,
  onCompleted?: any,
};

const TranslateSeconds = (input) => {
  const retVal = input;
  if (retVal
    && retVal.startTimeSeconds
    && (typeof retVal.startTimeSeconds === 'string' || retVal.startTimeSeconds instanceof String)
  ) {
    retVal.startTimeSeconds = Number.parseInt(retVal.startTimeSeconds, 10);
  }
  if (retVal
    && retVal.endTimeSeconds
    && (typeof retVal.endTimeSeconds === 'string' || retVal.endTimeSeconds instanceof String)
  ) {
    retVal.endTimeSeconds = Number.parseInt(retVal.endTimeSeconds, 10);
  }
  return retVal;
};

const Button = ({
  orgId,
  edit,
  currentTime,
  marker,
  ...props
}: Prop = {}) => {
  if (currentTime > 0) {
    // do somehting
  }
  return (
    <AddDialog
      {...props}
      initialValues={TranslateSeconds(marker)}
      inputs={inputs}
      mutation={edit ? updateMutation : mutation}
      title={edit ? 'Edit Marker' : 'Add Marker'}
      showClose
      translation={({
        id, description, endTimeSeconds, startTimeSeconds, name,
      }) => (
        {
          orgId,
          input: TranslateSeconds(
            {
              id,
              description,
              endTimeSeconds,
              startTimeSeconds,
              name,
            },
          ),
        }
      )}
      trigger={showDialog => (
        <div>
          <IconButton
            color="primary"
            onClick={showDialog}
          >
            {edit ? (
              <Edit />
            ) : (
              <Add />
            )}
          </IconButton>
          {edit ? (
            'Edit Marker'
          ) : (
            'Add Marker'
          )}
        </div>
      )}
    />
  );
};

export default Button;
